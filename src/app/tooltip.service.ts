import { Injectable, ComponentRef } from '@angular/core';
import { TooltipComponent } from './tooltip/tooltip.component';

@Injectable({
  providedIn: 'root'
})
export class TooltipService {

  private tooltips = {};
  constructor() { }


  getTooltipInstance(id: string) {
    if (id in this.tooltips && this.tooltips[id]) {
      return this.tooltips[id];
    }
    return false;
  }

  addTooltip(instance: ComponentRef<TooltipComponent>, id: string) {
    this.tooltips[id] = instance;
  }

  removeTooltip(id: string) {
    if (id in this.tooltips && this.tooltips[id]) {
      delete this.tooltips[id];
    }
  }
}
