import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TooltipComponent implements OnInit {

  content: string;
  isVisible: boolean;
  type: string;

  constructor() {
  }

  ngOnInit() {
  }

  public showComponent(): void {
    this.isVisible = true;
  }

  public hideComponent(): void {
    this.isVisible = false;
  }

}
