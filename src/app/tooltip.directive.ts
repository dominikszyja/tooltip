import {
  Directive, ViewContainerRef, ComponentFactoryResolver, Input, ElementRef,
  OnInit, Renderer2, HostListener, ComponentRef, Inject, OnDestroy, AfterViewInit, ChangeDetectorRef
} from '@angular/core';
import { TooltipComponent } from './tooltip/tooltip.component';
import { TooltipService } from './tooltip.service';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements OnInit, OnDestroy, AfterViewInit {

  @Input() appTooltip: string;
  @Input() appTooltipType: string;

  constructor(public elementRef: ElementRef, public viewContainerRef: ViewContainerRef,
    public componentFactoryResolver: ComponentFactoryResolver, public renderer: Renderer2,
    public tooltipService: TooltipService,
    public _changeDetectionRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.appTooltipType = this.appTooltipType || 'hover';

    if (!(this.appTooltipType === 'hover' || this.appTooltipType === 'click')) {
      this.appTooltipType = 'hover'
    }
    // this.appTooltipType = ['hover', 'click'].includes(this.appTooltipType) ? this.appTooltipType : 'hover';
    this.initTooltip();
  }

  initTooltip() {
    this.renderer.addClass(this.elementRef.nativeElement, 'tooltip-wrapper');
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(TooltipComponent);
    this.viewContainerRef.clear();

    const componentRef = this.viewContainerRef.createComponent(componentFactory);
    (componentRef.instance as TooltipComponent).content = this.appTooltip;
    (componentRef.instance as TooltipComponent).type = this.appTooltipType;

    this.renderer.appendChild(this.elementRef.nativeElement, componentRef.location.nativeElement);

    const id = Math.random().toString(36).substr(2, 9);

    this.renderer.setAttribute(this.elementRef.nativeElement, '_tooltip', id);
    this.tooltipService.addTooltip(componentRef, id);

    this._changeDetectionRef.detectChanges();
  }

  @HostListener('mouseenter') onMouseEnter() {
    if (this.appTooltipType === 'hover') {
      this.toggleTooltip();
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.appTooltipType === 'hover') {
      this.toggleTooltip(false);
    }
  }

  @HostListener('click') onClick() {
    if (this.appTooltipType === 'click') {
      this.toggleTooltip();
      this.renderer.setAttribute(this.elementRef.nativeElement, '_touched', 'true');
    }
  }

  @HostListener('document:click', ['$event.target'])
  public onOutsideClick(targetElement) {
    if (this.appTooltipType === 'click') {

      if (this.elementRef.nativeElement.getAttribute('_touched')) {
        const clickedInside = this.elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
          this.renderer.removeAttribute(this.elementRef.nativeElement, 'touched');
          this.toggleTooltip(false);
        }
      }
    }
  }

  toggleTooltip(show = true) {
    const id = this.elementRef.nativeElement.getAttribute('_tooltip');
    if (id) {
      const _componentRef: ComponentRef<TooltipComponent> = this.tooltipService.getTooltipInstance(id);
      return !_componentRef.instance.isVisible && show ? _componentRef.instance.showComponent() : _componentRef.instance.hideComponent();
    }
  }

  ngOnDestroy() {
    const id = this.elementRef.nativeElement.getAttribute('_tooltip');
    this.tooltipService.removeTooltip(id);
  }
}
