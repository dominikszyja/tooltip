# Tooltip


Zadanie testowe stworzenie klasycznego tooltipa. Zrealizowane w postaci komponentu, dyrektywy w Angular8

Wersja na żywo:

[Tooltip](https://domszyja.bitbucket.io/) (https://domszyja.bitbucket.io/)

## Zastosowanie:

Tooltip podpięty jako dyrektywa atrybutu: 'appTooltip', aby zastosować należy wprowadzić ten tag z odpowiednią treścią i opcjami:
Zgodnie ze specyfikacją tooltip można otwierać za pomocą kliknięcia, lub po najechaniu (w zaleznosci od podanego attrybutu [appTooltipType])

## Atrybuty

``` [appTooltip]='text' - stworzenie atrybutu o tresci wpisanej w miejscu 'text'
    [appTooltipType='click'] // dostępne opcje to: 'hover', 'click'
 ``` 

## Przykład

``` <p appTooltip='tresc tooltipu'> Normalny tekst </p>  ```
``` <p appTooltip='tresc tooltipu' appTooltipType='click'> Po nacisnięciu </p>  ```

## Urchomienie lokalnie

``` ng serve ```